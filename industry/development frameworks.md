# Development Frameworks

Truffle and Hardhat both use JavaScript. Truffle has been around for a while. The Truffle Suite ecosystem consists of three different tools: Truffle, Ganache, and Drizzle. HardHat is an Ethereum development environment that has tools and features that include deploying testing, compiling, and more. The third candidate is Brownie which uses Python.

## Truffle
Truffle is a development environment utilizing the EVM (Ethereum Virtual Machine) as a basis. The slogan of Truffle is ”Smart Contracts Made Sweeter”, indicating that the environment specializes in smart contract development. This environment features a number of great functionalities that help dApp developers tremendously. 

## Ganache
Ganache is a tool to set up your own local Ethereum blockchain that you can use to deploy and test your smart contracts/dApps before launching them on an authentic chain. As such, Ganache enables developers to avoid paying unnecessary gas fees during the developing process.

## Drizzle
Drizzle is a collection of frontend libraries with a Redux store as its basis. With Drizzle, frontend development can become a lot more predictable and manageable.

## Hardhat
Hardhat is a development environment that features testing, compiling, and debugging Ethereum-based dApps. This means that Hardhat is a helpful tool to enable and make some of the tasks inherent to dApp development more accessible. Hardhat comes with a built-in Ethereum network, and the platform focuses on Solidity debugging. This makes Hardhat an ideal tool for developing smart contracts and Ethereum dApps.

Furthermore, as Hardhat focuses on Solidity, some of the main features of this platform are Solidity stack traces and automatic error messages. However, these are just two features making the process of finding out where an application fails and how to solve this problem a whole lot easier. Along with focusing on debugging, Hardhat also features a vast selection of plugins. This makes the platform suitable for a high degree of customizability.

## Moralis
Moralis, the premier blockchain middleware, supercharges your dApp development and allows you to prototype, develop, and deploy dApps. Moralis removes the hurdle with dApp development, with its scalable backend infrastructure. This allows users to save resources and time, which is excellent for business. Moreover, Moralis also offers several useful tools, such as Speedy Nodes and support for Ganache. 