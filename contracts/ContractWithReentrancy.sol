// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract ContractWithReentrancy {

    uint256 private balance;

    function deposit() public payable {
        balance += msg.value;
    }

    function withdraw() public {
        uint256 amount = 1 ether;
        require(amount < balance);
        //
        // The call function on address data type can call public and external
        // functions on contracts. It can also to be used to transfer ether to
        // addresses.
        //
        // call is not recommended in most situations because it bypasses type
        // checking, function existence check and argument packing. It is preferred
        // to import the interface of the contract and call functions on it.
        //
        // Receive is called when no data is sent in the function call and ether is 
        // sent. Fallback function is called when no function signature matches 
        // the call.
        //
        // call consumes less gas than calling the function on the contract instance.
        // In some cases, call is preferred for gas optimisation.
        //
        // For 
        // function f(uint _x, address _addr) public returns(uint, uint) {
        //    return (a, b);
        // }
        // The call is like this 
        //    (bool success, bytes memory result) = addr.call(
        //    abi.encodeWithSignature("f(uint,address)", 
        //    10, 
        //    msg.sender);
        //        
        (bool success,) = msg.sender.call{value:amount}("");
        require(success, "Transfer failed.");
        balance -= amount;

    }

    function getBalance() public view returns (uint256) {
        return balance;
    }

}