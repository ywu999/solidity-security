// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

contract Ledger {
    
    mapping(address => uint256) public balances;

    function deposit() public payable {
        balances[msg.sender] += msg.value;
    }

     function withdraw() external {
        uint256 amount = balances[msg.sender];
        (bool success,) = msg.sender.call{value:amount}("");
        require(success, "Withdraw failed.");
        balances[msg.sender] = 0;
    }
    // Use Checks-effects-interactions pattern to mitigate the reentrancy attacks
    // First perform any checks, which are normally assert or require statements
    // If the check passes, the function should then resolve all the effects on the state of the contract
    // Only after all state changes are resolved should the function interact with other contracts.
    //
    // function withdraw() external {
    //    uint256 amount = balances[msg.sender];
    //    balances[msg.sender] = 0;
    //    (bool success,) = msg.sender.call{value:amount}("");
    //    require(success, "Withdraw failed.");
    // }

    function transfer(address to, uint256 amount) external {
        require(balances[msg.sender] >= amount);
        balances[to] += amount;
        balances[msg.sender] -= amount;
    }
}
